import {TestBed} from '@angular/core/testing';
import {HttpClient} from '@angular/common/http';
import {HttpRequest} from '@dkx/json-api/lib/http';
import {of} from 'rxjs';

import {AngularFetch} from './angular-fetch.service';


describe('AngularFetch', () => {
	let http: jasmine.SpyObj<HttpClient>;
	let fetch: AngularFetch;

	beforeEach(() => {
		TestBed.configureTestingModule({});

		http = jasmine.createSpyObj('HttpClient', ['request']);
		fetch = new AngularFetch(http);
	});

	it('should call request', async () => {
		http.request.and.returnValue(of());
		await fetch.request(new HttpRequest('GET', ''));
		expect(http.request.calls.count()).toBe(1);
	});
});
