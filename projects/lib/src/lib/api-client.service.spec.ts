import {TestBed} from '@angular/core/testing';
import {ApiClient as BaseApiClient} from '@dkx/json-api';
import {HttpResponse} from '@dkx/json-api/lib/http';

import {NgApiClient} from './api-client.service';


describe('ApiClient', () => {
	let baseClient: jasmine.SpyObj<BaseApiClient>;
	let client: NgApiClient;

	beforeEach(() => {
		TestBed.configureTestingModule({});

		baseClient = jasmine.createSpyObj('BaseApiClient', ['getRaw', 'putRaw', 'postRaw', 'delete']);
		client = new NgApiClient(baseClient);
	});

	it('should call get', (done) => {
		baseClient.getRaw.and.returnValue(Promise.resolve(new HttpResponse()));
		client.getRaw('').subscribe(() => {
			expect(baseClient.getRaw.calls.count()).toBe(1);
			done();
		});
	});

	it('should call put', (done) => {
		baseClient.putRaw.and.returnValue(Promise.resolve(new HttpResponse()));
		client.putRaw('', {}).subscribe(() => {
			expect(baseClient.putRaw.calls.count()).toBe(1);
			done();
		});
	});

	it('should call post', (done) => {
		baseClient.postRaw.and.returnValue(Promise.resolve(new HttpResponse()));
		client.postRaw('', {}).subscribe(() => {
			expect(baseClient.postRaw.calls.count()).toBe(1);
			done();
		});
	});

	it('should call delete', (done) => {
		baseClient.delete.and.returnValue(Promise.resolve(new HttpResponse()));
		client.delete('').subscribe(() => {
			expect(baseClient.delete.calls.count()).toBe(1);
			done();
		});
	});
});
