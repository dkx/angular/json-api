import {ApiClient, ApiRequestOptions, HttpResponseInterface, MiddlewareInterface} from '@dkx/json-api';
import {ClassType} from '@dkx/types-class';
import {Observable, from as ObservableFrom} from 'rxjs';


export class NgApiClient
{
	constructor(
		private readonly baseClient: ApiClient,
	) {}

	public use(middleware: MiddlewareInterface): void
	{
		this.baseClient.use(middleware);
	}

	public getRaw<TResult>(url: string, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<TResult>>
	{
		return ObservableFrom(this.baseClient.getRaw<TResult>(url, options));
	}

	public getOne<TEntity>(mapTo: ClassType<TEntity>, url: string, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<TEntity|undefined>>
	{
		return ObservableFrom(this.baseClient.getOne<TEntity>(mapTo, url, options));
	}

	public getMany<TEntity>(mapTo: ClassType<TEntity>, url: string, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<Array<TEntity>>>
	{
		return ObservableFrom(this.baseClient.getMany<TEntity>(mapTo, url, options));
	}

	public putRaw<TResult>(url: string, body: object, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<TResult>>
	{
		return ObservableFrom(this.baseClient.putRaw<TResult>(url, body, options));
	}

	public put<TEntity>(mapTo: ClassType<TEntity>, url: string, body: object, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<TEntity|undefined>>
	{
		return ObservableFrom(this.baseClient.put<TEntity>(mapTo, url, body, options));
	}

	public postRaw<TResult>(url: string, body: object, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<TResult>>
	{
		return ObservableFrom(this.baseClient.postRaw<TResult>(url, body, options));
	}

	public post<TEntity>(mapTo: ClassType<TEntity>, url: string, body: object, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<TEntity|undefined>>
	{
		return ObservableFrom(this.baseClient.post<TEntity>(mapTo, url, body, options));
	}

	public delete<TResult = void>(url: string, options: ApiRequestOptions = {})
		: Observable<HttpResponseInterface<TResult>>
	{
		return ObservableFrom(this.baseClient.delete<TResult>(url, options));
	}
}
