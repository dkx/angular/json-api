import {HttpClient} from '@angular/common/http';
import {FetchInterface, HttpRequestInterface} from '@dkx/json-api';


export class AngularFetch implements FetchInterface
{
	constructor(
		private readonly http: HttpClient,
	) {}

	public request<TResult>(request: HttpRequestInterface): Promise<TResult>
	{
		return this.http
			.request<TResult>(request.method, request.url, {
				body: request.body,
				responseType: 'json',
			})
			.toPromise();
	}
}
