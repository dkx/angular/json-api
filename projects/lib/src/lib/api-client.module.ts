import {NgModule, ModuleWithProviders, Optional} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ApiClient, ApiClientOptions, FetchInterface, MiddlewareInterface} from '@dkx/json-api';

import {AngularFetch} from './angular-fetch.service';
import {NgApiClient} from './api-client.service';
import {FETCH_TOKEN, MIDDLEWARE_TOKEN, OPTIONS_TOKEN} from './injection-tokens';


export function createFetch(httpClient: HttpClient): FetchInterface
{
	return new AngularFetch(httpClient);
}

export function createApiClient(
	fetch: FetchInterface,
	options: ApiClientOptions,
	middlewares: ReadonlyArray<MiddlewareInterface>|null,
): ApiClient
{
	const client = new ApiClient(fetch, options);

	if (middlewares !== null) {
		for (const middleware of middlewares) {
			client.use(middleware);
		}
	}

	return client;
}

export function createNgApiClient(baseClient: ApiClient): NgApiClient
{
	return new NgApiClient(baseClient);
}

@NgModule({
	imports: [
		HttpClientModule,
	],
})
export class ApiClientModule
{
	public static forRoot(options: ApiClientOptions = {}): ModuleWithProviders<ApiClientModule>
	{
		return {
			ngModule: ApiClientModule,
			providers: [
				{
					provide: OPTIONS_TOKEN,
					useValue: options,
				},
				{
					provide: FETCH_TOKEN,
					deps: [HttpClient],
					useFactory: createFetch,
				},
				{
					provide: ApiClient,
					deps: [FETCH_TOKEN, OPTIONS_TOKEN, [new Optional(), MIDDLEWARE_TOKEN]],
					useFactory: createApiClient,
				},
				{
					provide: NgApiClient,
					deps: [ApiClient],
					useFactory: createNgApiClient,
				},
			],
		};
	}
}
