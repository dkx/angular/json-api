import {InjectionToken} from '@angular/core';


export const OPTIONS_TOKEN = new InjectionToken('jsonapi/options');
export const FETCH_TOKEN = new InjectionToken('jsonapi/fetch');
export const MIDDLEWARE_TOKEN = new InjectionToken('jsonapi/middleware');
