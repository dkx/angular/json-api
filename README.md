# DKX/NgJsonApi

Implementation of [@dkx/json-api](https://gitlab.com/dkx/node.js/json-api) for angular. Look there for main documentation.

## Installation

```bash
$ npm install --save @dkx/ng-json-api 
```

## Register module

Registering the ng-json-api module is simple enough, just provide the decorator mapping loader and import 
`HttpClientModule` from `@angular/common/http`:

```typescript
import {NgModule} from '@angular/core';
import {ApiClientModule} from '@dkx/ng-json-api';

@NgModule({
    imports: [
        ApiClientModule.forRoot({
            baseUrl: 'http://localhost:8080',
        }),
    ],
})
export class AppModule {}
```

## Api client

Example in custom service:

```typescript
import {Injectable} from '@angular/core';
import {NgApiClient} from '@dkx/ng-json-api';
import {Observable} from 'rxjs';
import {User} from './user';

@Injectable({
    providedIn: 'root',
})
export class UsersService
{
    constructor(
        private readonly api: NgApiClient,
    ) {}

    public getById(id: string): Observable<User>
    {
        return this.api.getOne<User>(User, `http://localhost:8080/v1/users/${id}`);
    }

    public getAll(): Observable<Array<User>>
    {
        return this.api.getMany<User>(User, 'http://localhost:8080/v1/users');
    }
}
```
